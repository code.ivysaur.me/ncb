# ncb

![](https://img.shields.io/badge/written%20in-Bash%2C%20Awk-blue)

A DSL for writing condensed nginx configurations.

ncb (Nginx Configuration Builder) is a scaffolding shell script aimed at reducing nginx configurations.

Tags: PL, sysadmin

## Usage

Produce (and store under source control) a definition script:


```
buildall() {
  # An example host:
  (
    allowcors
    staticonly "/srv/www/code.ivysaur.me/wwwroot"
    serve "/example"
  ) | vhost "$HOST" "code.ivysaur.me"
  # other host definitions...
}
```


Generate nginx configurations with `build-nginx-config.sh path/to/above`.

## Changelog

2015-12-13: v1.1.0
- Feature: ACME support for Let's Encrypt (enable by defining `$ACME_CHALLENGE_DIR`)
- Feature: gzip support (disable with comment `@NO_GZIP_HEADER`)
- Feature: New `makedefault`, `vhost_sslredir`, `serve` directives
- Compatibility: Remove `downloaddir`
- Always indicate UTF-8 character set in Content-Type
- Improve performance on non-Linux platforms by preferring non-forking subshells
- [⬇️ ncb_v1.1.0.tar.xz](dist-archive/ncb_v1.1.0.tar.xz) *(1.85 KiB)*


2014-08-17: v1.0.0
- Initial release
- [⬇️ ncb_v1.0.0.tar.gz](dist-archive/ncb_v1.0.0.tar.gz) *(1.82 KiB)*

